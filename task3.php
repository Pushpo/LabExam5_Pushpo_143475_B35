<?php
class MyCalculator
{
    public $_val1;
    public $_val2;
    public function __construct($val1,$val2)
    {
        $this->_val1=$val1;
        $this->_val2=$val2;
    }
    public function add(){
        return $this->_val1+$this->_val2;
    }
    public function subtract(){
        return $this->_val1-$this->_val2;
    }
    public function multiply(){
        return $this->_val1*$this->_val2;
    }
    public function divide(){
        return $this->_val1/$this->_val2;
    }
}
$mycalc = new MyCalculator( 12, 6);

echo "Addition of (12,6) is : " . $mycalc-> add() ."<br>";
echo "Subtraction of (12,6) is : " . $mycalc-> subtract()  ."<br>";
echo "Multiplication of (12,6) is : " . $mycalc-> multiply() ."<br>";
echo "Division of (12,6) is : " . $mycalc-> divide() ."<br>";
?>